﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScroll : MonoBehaviour
{
    public enum ScrollAxis {x,y,};
    public ScrollAxis Axis = ScrollAxis.x;
    public float Speed = 0f;
    Vector2 vecotor;
    Renderer renderer;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        switch (Axis)
        {
            case ScrollAxis.x:
                vecotor = Vector2.right;
                break;
            case ScrollAxis.y:
                vecotor = Vector2.up;
                break;
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        renderer.material.mainTextureOffset += vecotor * Time.deltaTime * Speed;
    }
}
