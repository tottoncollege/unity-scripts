﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCharacterController : MonoBehaviour
{
    CharacterController characterController;

    public float speed = 12.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    Vector3 move;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (characterController.isGrounded)
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            // We are grounded, so recalculate
            // move direction directly from axes

            move = transform.right * x + transform.forward * z;
            move *= speed;

            if (Input.GetButton("Jump"))
            {
                move.y = jumpSpeed;
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        move.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(move * Time.deltaTime);
    }

}
