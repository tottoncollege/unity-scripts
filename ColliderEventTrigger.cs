﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderEventTrigger : MonoBehaviour
{

    public UnityEvent TargetFunctiontion;
    public string TagToTrigger = "Player";

    private void Start()
    {
        if (TargetFunctiontion == null)
            TargetFunctiontion = new UnityEvent();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(TagToTrigger))
            TriggerEvent();
    }

    private void OnCollisionEnter (Collision other)
    {
        if (other.gameObject.CompareTag(TagToTrigger))
            TriggerEvent();
    }

    void TriggerEvent ()
    {
        TargetFunctiontion.Invoke();
    }
}
